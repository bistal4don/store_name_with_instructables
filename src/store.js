import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import _flattenDeep from 'lodash/flattenDeep'
import {userReducer as user, userMiddleware} from './client/modules/user'

const reducer = combineReducers({
    user
})

const middleware = applyMiddleware.apply(null, _flattenDeep([
    userMiddleware
]))



const middlewareEnhancer = compose(middleware);
const store = createStore(reducer, middlewareEnhancer);
export default store;