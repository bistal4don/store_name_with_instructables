/* eslint-disable notice/notice */
import React, { Component } from 'react'

// canvas
import { themeable } from '@instructure/ui-themeable'
import { View } from '@instructure/ui-view'

// local files
import styles from './styles.css'
import theme from './theme.js'
import UpdateName from './nameData/UpdateName'
import DisplayName from './nameData/DisplayName'
import DeleteName from './nameData/DeleteName'
import Confirmation from './comfirmation/Confirmation'

// redux
import { connect } from 'react-redux'

class HomePage extends Component {

  render() {
    const { isSaved } = this.props

    if(isSaved.result) {
      return <Confirmation name={isSaved.result.name} />
    }

    return (
      <View
        as="main"
        padding="large medium none"
        maxWidth="100%"
        minHeight="100%"
      >
        <UpdateName />
        <hr />

        <DisplayName />
        <hr />

        <DeleteName />
      </View>
    )
  }
}

const styledComponent = themeable(theme, styles)(HomePage)

const mapStateToProps = (state) => {
  return {
    isSaved: state.user.isSaved || {}
  }
}

export default connect(mapStateToProps, null)(styledComponent)
