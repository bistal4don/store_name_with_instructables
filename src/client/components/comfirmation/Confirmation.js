/* eslint-disable notice/notice */
import React, { Component } from 'react'

// canvas
import { themeable } from '@instructure/ui-themeable'
import { Heading } from '@instructure/ui-heading'
import { Text } from '@instructure/ui-text'
import { View } from '@instructure/ui-view'
import { Button } from '@instructure/ui-buttons'
import { IconCheckMarkSolid } from '@instructure/ui-icons'

// local fiels
import styles from '../styles.css'
import theme from '../theme.js'

class Confirmation extends Component {

  render() {
    const { name } = this.props
    return (
        <View
        as="main"
        background="primary-inverse"
        padding="large medium none"
        minHeight="100%"
        textAlign="center"
      >
        <View
          padding="small"
          display="inline-block"
          background="success"
          borderRadius="large"
          shadow="topmost"
        >
          <IconCheckMarkSolid size="medium" inline={false} />
        </View>
        <div className={styles.banner}>
          <View
            maxWidth="40rem"
            margin="0 auto"
            padding="x-large medium medium"
            display="block"
            background="light"
            borderRadius="large"
            shadow="above"
          >
            <Heading level="h1" margin="none none small">
              Your name has been updated to
              <Text weight="bold" size="xx-large"> {name} </Text>
            </Heading>
          </View>

          <Button color="primary" margin="small none" onClick={() => location.reload()}>
            OK
         </Button>
        </div>
      </View>
    )
  }
}

const styledComponent = themeable(theme, styles)(Confirmation)
export default styledComponent
