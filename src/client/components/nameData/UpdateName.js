/* eslint-disable notice/notice */
import React, { Component } from 'react'

// canvas
import { themeable } from '@instructure/ui-themeable'
import { TextInput } from '@instructure/ui-text-input'
import { View } from '@instructure/ui-view'
import { Button } from '@instructure/ui-buttons'
import { ProgressCircle } from '@instructure/ui-progress'

// local files
import styles from '../styles.css'
import theme from '../theme.js'

// redux
import { storeName } from '../../action/user'
import { connect } from 'react-redux'

class UpdateName extends Component {
  state = {
    name: '',
    emptyText: false,
    errorMessage: []
  }

  handleChange = (event) => {
    const {
      target: { name, value },
    } = event
    this.setState({ [name]: value })

    // remove the error message
    this.setState({errorMessage: []})
  }

  handleClick = () => {
    const { name } = this.state
    const { storeName } = this.props

    // only save a users name when they actually entered something
    if(name) {
      storeName(name)
    } else {
      this.setState({errorMessage: [{text: 'Please provide a name before submission', type: 'error'}]})
    }
  }



  render() {
    const { name, errorMessage } = this.state
    const { isSaved } = this.props

    if(isSaved.loading) {
        return <ProgressCircle
        screenReaderLabel="Loading completion"
        color="primary-inverse"
        valueNow={50}
        valueMax={60}
        />
    }

    return (
      <View
        as="main"
        padding="large medium none"
        maxWidth="40rem"
        minHeight="100%"
      >
        <div className={styles.banner}>
          <TextInput
            renderLabel="Name"
            value={name}
            isRequired={!!name}
            name="name"
            onChange={this.handleChange}
            messages={errorMessage}
            shouldNotWrap
          />
          <Button color="primary" margin="small none" onClick={this.handleClick}>
            Save
          </Button>
        </div>
      </View>
    )
  }
}

const styledComponent = themeable(theme, styles)(UpdateName)

const mapStateToProps = (state) => {
  return {
    isSaved: state.user.isSaved || {}
  }
}

export default connect(mapStateToProps, {storeName})(styledComponent)
