/* eslint-disable notice/notice */
import React, { Component } from 'react'

// canvas
import { themeable } from '@instructure/ui-themeable'
import { View } from '@instructure/ui-view'
import { Button } from '@instructure/ui-buttons'

// local files
import styles from '../styles.css'
import theme from '../theme.js'

// redux
import { removeName } from '../../action/user'
import { connect } from 'react-redux'

class DeleteName extends Component {

  handleClick = () => {
    const { removeName } = this.props
    removeName()
    location.reload()
  }

  render() {
    return (
      <View
        as="main"
        padding="large medium none"
        maxWidth="40rem"
        minHeight="100%"
      >
         <Button color="danger" margin="small none" onClick={this.handleClick}>
            Delete
         </Button>
      </View>
    )
  }
}

const styledComponent = themeable(theme, styles)(DeleteName)

const mapStateToProps = (state) => {
  return {
    userName: state.user.userName || {}
  }
}

export default connect(mapStateToProps, {removeName})(styledComponent)
