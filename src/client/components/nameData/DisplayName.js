/* eslint-disable notice/notice */
import React, { Component } from 'react'

// canvas
import { themeable } from '@instructure/ui-themeable'
import { Text } from '@instructure/ui-text'
import { View } from '@instructure/ui-view'

// local files
import styles from '../styles.css'
import theme from '../theme.js'

// redux
import { getName } from '../../action/user'
import { connect } from 'react-redux'

class UpdateName extends Component {

  componentDidMount(prevProps, prevState) {
    const { getName} = this.props
    getName()
  }

  render() {
    const { userName: {result}  } = this.props

    if(result) {
      return  <View
      as="main"
      padding="large medium small"
      maxWidth="40rem"
      minHeight="100%"
    >
      <div className={styles.banner}>
          <Text size="large"> Your Current saved name is: </Text>
          <Text weight="bold" size="x-large"> {result.name} </Text>
      </div>
    </View>
    }

    return (
      <View
        as="main"
        padding="large medium none"
        maxWidth="40rem"
        minHeight="100%"
      >
        <div className={styles.banner}>
            <Text weight="bold" size="large"> No name has been saved yet </Text>
        </div>
      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    userName: state.user.userName || {}
  }
}

const styledComponent = themeable(theme, styles)(UpdateName)
export default connect(mapStateToProps, {getName})(styledComponent)
