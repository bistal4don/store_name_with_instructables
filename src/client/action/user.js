/* eslint-disable notice/notice */
// import { ACTION_RESET_SUFFIX } from '../utils/genericReducer'
const PREFIX = 'user'
export const STORE_NAME = PREFIX + ':name:set'
export const REMOVE_NAME = PREFIX + ':name:clear'
export const GET_NAME = PREFIX + ':name:get'

export const STORED_NAME = PREFIX + ':name:set:updated'

export function storeName(value) {
  return {
    type: STORE_NAME,
    name: value,
  }
}

export function getName() {
  return {
    type: GET_NAME,
  }
}

export function removeName() {
  return {
    type: REMOVE_NAME,
  }
}
