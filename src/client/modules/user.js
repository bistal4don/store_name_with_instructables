/* eslint-disable notice/notice */
import { combineReducers } from 'redux'
import { STORE_NAME, REMOVE_NAME, GET_NAME, STORED_NAME } from '../action/user'
import {
  createActionMiddleware, createLoadingMiddleware,
} from '../utils/genericMiddleware'
import { createActionReducer, createLoadingReducer } from '../utils/genericReducer'


export const userReducer = combineReducers({
  userName: createActionReducer(GET_NAME),
  isSaved: createLoadingReducer(STORE_NAME, STORED_NAME),
})

const getUserNameUrl = () => {
  return {
    url: '/api/user/',
    method: 'GET',
  }
}

const removeUserNameUrl = () => {
  return {
    url: '/api/user/',
    method: 'DELETE',
  }
}

const saveUserNameUrl = (action) => {
  return {
    url: '/api/user/',
    method: 'PUT',
    body: {
      name: action.name,
    },
  }
}

export const userMiddleware = [
  createActionMiddleware(GET_NAME,  getUserNameUrl),
  createActionMiddleware(REMOVE_NAME, removeUserNameUrl),
  createLoadingMiddleware(STORE_NAME, STORED_NAME, saveUserNameUrl),
]
