/* eslint-disable notice/notice */
const express = require('express')
const log = require('bunyan').createLogger({ name: 'user' })
const { config } = require('../../utils/config')
const { requestAPI } = require('../../utils/requestAPI')
const { apiUrl } = config

exports.setup = () => {
  const router = express.Router()

  router.put('/', async (req, res) => {
    try {
      const response = await requestAPI({
        url: apiUrl + '/update',
        method: 'POST',
        json: req.body,
      })
      log.info('User name saved', response)
      res.send(response)
    } catch (err) {
      log.error({ err }, 'Got error retrieving request')
    }
  })

  router.get('/', async (req, res) => {
    try {
      const response = await requestAPI({ url: apiUrl + '/get' })
      log.info('User name gotten', response)
      res.send(response)
    } catch (err) {
      log.error({ err }, 'Got error retrieving request')
    }
  })

  router.delete('/', async (req, res) => {
    try {
      const response = await requestAPI({
        url: apiUrl + '/clear',
        method: 'POST',
      })
      log.info('User name deleted', response)
      res.send(response)
    } catch (err) {
      log.error({ err }, 'Got error retrieving request')
    }
  })

  return router
}
