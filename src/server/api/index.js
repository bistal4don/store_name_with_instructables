/* eslint-disable notice/notice */
const express = require('express')

const user = require('./user/user')

exports.setup = () => {
  const router = express.Router()

  router.use('/user', user.setup())
  return router
}
