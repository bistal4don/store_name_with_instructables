/* eslint-disable notice/notice */
const request = require('request-promise-native')

exports.requestAPI = async function requestAPI(requestConfig) {
  return request(requestConfig)
}
