/* eslint-disable notice/notice */
'use strict'
const express = require('express')
const cors = require('cors')
const path = require('path')
const index = require('./api/index')
const log = require('bunyan').createLogger({ name: 'user' })
const { config } = require('./utils/config')
const { port } = config

// using imports in our app
const app = express()
app.use(express.json())
app.use(cors())

// route middlewares
app.use('/api', index.setup())

// deafault page
app.get('/', function (req, res) {
  res.sendFile('index.html', { root: path.join(__dirname, './html') })
})

app.listen(port, () => log.info('Server - I am now running on port ', port))
